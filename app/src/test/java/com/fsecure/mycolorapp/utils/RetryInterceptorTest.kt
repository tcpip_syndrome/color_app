package com.fsecure.mycolorapp.utils

import com.fsecure.mycolorapp.utils.RetrofitClient.Companion.RETRY_COUNT
import com.fsecure.mycolorapp.utils.RetrofitClient.Companion.retryInterceptor
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import okhttp3.Interceptor
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.Response
import org.junit.Test

class RetryInterceptorTest {

    private val TEST_REQEUST_URL = "https://logcat.net"

    @Test
    fun testRetryInterceptorSuccess() {
        val chain = mockk<Interceptor.Chain>()
        val request = Request.Builder().url(TEST_REQEUST_URL).build()
        every { chain.request() } returns request

        val a200Response =
            Response.Builder().request(request).protocol(Protocol.HTTP_2).message("").code(200)
                .build()
        every { chain.proceed(request) } returns a200Response
        RetrofitClient.retryInterceptor.intercept(chain)
        verify(exactly = 1) { chain.proceed(request) }
    }

    @Test
    fun testRetryInterceptorRetry() {
        val chain = mockk<Interceptor.Chain>()
        val request = Request.Builder().url(TEST_REQEUST_URL).build()
        every { chain.request() } returns request

        val aRetryableResponse =
            Response.Builder().request(request).protocol(Protocol.HTTP_2).message("")
                .code(Constant.RETRY_STATUS_CODES[0]).build()
        every { chain.proceed(request) } returns aRetryableResponse
        RetrofitClient.retryInterceptor.intercept(chain)
        verify(exactly = RETRY_COUNT + 1) { chain.proceed(request) }

    }

}