/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.fsecure.mycolorapp.models.Color
import com.fsecure.mycolorapp.models.ResponseObject
import com.fsecure.mycolorapp.models.Storage
import com.fsecure.mycolorapp.repository.StorageRepository
import com.fsecure.mycolorapp.utils.AppPreference

class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    private var storageRepository: StorageRepository
    private var appPreference: AppPreference

    val showProgressBar: MutableLiveData<Boolean> =
        MutableLiveData<Boolean>().apply { false }

    val openLoginActivity: MutableLiveData<Boolean> =
        MutableLiveData<Boolean>().apply { false }

    init {
        storageRepository = StorageRepository.getInstance(context)
        appPreference = AppPreference.getInstance(context)
    }

    val colors: MutableLiveData<List<Color>> =
        MutableLiveData<List<Color>>(emptyList())

    val showEmpty = MediatorLiveData<Boolean>()

    init {
        val onChanged = {
            val isLoading = showProgressBar.value!!
            val hasColors = colors.value?.isNotEmpty() ?: false
            showEmpty.value = !isLoading && !hasColors
        }
        showEmpty.addSource(colors) { onChanged() }
        showEmpty.addSource(showProgressBar) { onChanged() }
    }

    fun getStorage(): LiveData<ResponseObject<Storage>> {
        var storageLiveData: MutableLiveData<ResponseObject<Storage>> = MutableLiveData()
        val dataId = appPreference.getLong(AppPreference.Keys.DATA_ID.name)
        if (dataId != 0L) {
            showProgressBar.value = true
            storageLiveData = storageRepository.getStorage(dataId)
        }
        showProgressBar.value = false
        return storageLiveData
    }

    fun updateStorage(colors: List<Color>): MutableLiveData<ResponseObject<Storage>> {
        showProgressBar.value = true
        val dataId = appPreference.getLong(AppPreference.Keys.DATA_ID.name)
        val result = if (dataId != 0L) {
            storageRepository.updateStorage(dataId, colors)
        } else {
            storageRepository.createStorage(colors)
        }
        showProgressBar.value = false
        return result
    }

    fun onStorageUpdated(storage: Storage) {
        val dataId = appPreference.getLong(AppPreference.Keys.DATA_ID.name)
        if (dataId == 0L) {
            appPreference.putLong(AppPreference.Keys.DATA_ID.name, storage.id)
        }
        updateColorsList(storage.data)
    }

    fun updateColorsList(colors: List<Color>) {
        this.colors.value = colors
    }
}