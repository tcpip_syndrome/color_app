/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.fsecure.mycolorapp.models.ResponseObject
import com.fsecure.mycolorapp.repository.UserRepository
import com.fsecure.mycolorapp.utils.AppPreference

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext

    val username: MutableLiveData<String> by lazy {
        MutableLiveData<String>().apply { value = "" }
    }
    val password: MutableLiveData<String> by lazy {
        MutableLiveData<String>().apply { value = "" }
    }
    val showProgressBar: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>().apply { false }
    }

    val openMainActivity: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>().apply { false }
    }

    fun login(): MutableLiveData<ResponseObject<String>> {
        showProgressBar.value = true
        return UserRepository.getInstance().login(username.value!!, password.value!!)
    }

    fun onUserLoggedIn(token: String) {
        showProgressBar.value = false
        AppPreference.getInstance(context)
            .putString(AppPreference.Keys.TOKEN.name, token)
        openMainActivity.value = true
    }

}