/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.`interface`

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface UserService {
    @POST("v1/login")
    fun login(@Body body: HashMap<String, String>): Call<JsonObject>
}