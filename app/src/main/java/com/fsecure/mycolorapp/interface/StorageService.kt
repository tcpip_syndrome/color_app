/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.`interface`

import com.fsecure.mycolorapp.models.Color
import com.fsecure.mycolorapp.models.Storage
import com.fsecure.mycolorapp.models.StorageCreateRequest
import com.fsecure.mycolorapp.models.StorageUpdateRequest
import retrofit2.Call
import retrofit2.http.*

interface StorageService {
    @POST("v1/storage")
    fun createStorage(@Header("Content-Type")
                      contentType: String?,
                      @Header("Authorization")
                      token: String,
                      @Body
                      body: StorageCreateRequest) : Call<Storage>

    @PUT("v1/storage/{id}")
    fun updateStorage(@Header("Content-Type")
                      contentType: String?,
                      @Header("Authorization")
                      token: String,
                      @Body
                      body: StorageUpdateRequest,
                      @Path("id")
                      id: Long) : Call<Storage>

    @GET("v1/storage/{id}")
    fun getStorage(@Header("Content-Type")
                   contentType: String?,
                   @Header("Authorization")
                   token: String,
                   @Path("id") id: Long) : Call<Storage>

    @DELETE("v1/storage/{id}")
    fun deleteStorage(@Header("Content-Type")
                      contentType: String?,
                      @Header("Authorization")
                      token: String,
                      @Path("id")
                      id: Long) : Call<String>
}