/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.fsecure.mycolorapp.R
import com.fsecure.mycolorapp.viewmodel.LoginViewModel
import androidx.lifecycle.ViewModelProviders
import com.fsecure.mycolorapp.databinding.ActivityLoginBinding
import kotlinx.android.synthetic.main.activity_login.*
import com.fsecure.mycolorapp.utils.ApiResponseHelper

class LoginActivity : AppCompatActivity() {
    private lateinit var viewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.setLifecycleOwner(this)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        binding.viewModel = viewModel
        login_button.setOnClickListener {
            viewModel.login().observe(this, Observer {
                progress_view.visibility = View.GONE
                ApiResponseHelper.handleApiResponse(this, it) { data: String ->
                    viewModel.onUserLoggedIn(data)
                }
            })
        }

        viewModel.showProgressBar.observe(this, {
            if (it == true) {
                progress_view.visibility = View.VISIBLE
            } else {
                progress_view.visibility = View.GONE
            }
        })

        viewModel.openMainActivity.observe(this, {
            if (it == true) {
                startActivity(Intent(this, MainActivity::class.java))
                this.finish()
            }
        })
    }

}
