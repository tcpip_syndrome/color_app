/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.activity

import android.content.Intent
import android.graphics.Color.*
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.fsecure.mycolorapp.R
import com.fsecure.mycolorapp.databinding.MainActivityBinding
import com.fsecure.mycolorapp.models.Color
import com.fsecure.mycolorapp.models.Storage
import com.fsecure.mycolorapp.utils.ApiResponseHelper
import com.fsecure.mycolorapp.viewmodel.MainViewModel
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.pes.androidmaterialcolorpickerdialog.ColorPicker
import kotlinx.android.synthetic.main.color_selector_view.*
import kotlinx.android.synthetic.main.color_selector_view.view.*
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: MainActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.main_activity)
        binding.setLifecycleOwner(this)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.getStorage().observe(this, {
            ApiResponseHelper.handleApiResponse(this, it) { storage: Storage ->
                viewModel.updateColorsList(storage.data)
            }
        })
        val adapter = ColorsAdapter()
        palettes_grid.adapter = adapter
        viewModel.colors.observe(this, { colors ->
            adapter.setColors(colors)
        })

        viewModel.openLoginActivity.observe(this, {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        })

        create_color.setOnClickListener {
            val currentColor = viewModel.colors.value?.firstOrNull()
            val cp =
                if (currentColor == null)
                    ColorPicker(this@MainActivity, 64, 128, 192)
                else {
                    ColorPicker(
                        this@MainActivity,
                        currentColor.red(),
                        currentColor.green(),
                        currentColor.blue()
                    )
                }

            cp.show()
            cp.enableAutoClose()
            cp.setCallback { colorCode ->
                val color = Color(Integer.toHexString(colorCode))
                val newColors = viewModel.colors.value!! + color
                viewModel.updateStorage(newColors)
                    .observe(this, {
                        ApiResponseHelper.handleApiResponse(this, it) { storage: Storage ->
                            viewModel.onStorageUpdated(storage)
                        }
                    })
            }
        }
    }
}