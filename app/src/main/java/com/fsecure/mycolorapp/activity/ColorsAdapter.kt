package com.fsecure.mycolorapp.activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fsecure.mycolorapp.R
import com.fsecure.mycolorapp.models.Color
import kotlinx.android.synthetic.main.item_palette.view.*

class ColorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}

class ColorsAdapter : RecyclerView.Adapter<ColorViewHolder>() {

    private class Diffs(val oldItems: List<Color>, val newItems: List<Color>) :
        DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition].equals(newItems[newItemPosition])
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition].equals(newItems[newItemPosition])
        }
    }

    private var colors = emptyList<Color>()

    fun setColors(newColors: List<Color>) {
        val diff = DiffUtil.calculateDiff(Diffs(colors, newColors))
        colors = newColors
        diff.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        return ColorViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_palette, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        colors[position].androidColorCode()?.let {
            holder.itemView.palette.setBackgroundColor(it)
        }
    }

    override fun getItemCount(): Int {
        return colors.size
    }
}