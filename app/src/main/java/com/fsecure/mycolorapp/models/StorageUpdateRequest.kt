package com.fsecure.mycolorapp.models

data class StorageUpdateRequest(val data: List<Color>)