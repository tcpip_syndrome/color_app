/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.models

data class Storage(var id: Long, var data: List<Color>)