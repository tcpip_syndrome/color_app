/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.models

import android.graphics.Color

data class Color(val code: String) {

    @Transient
    private var colorCode: Int? = null

    fun androidColorCode(): Int? {
        if (colorCode == null) {
            colorCode = android.graphics.Color.parseColor("#" + code)
        }
        return colorCode
    }

    fun red(): Int {
        return colorChannel(android.graphics.Color::red)
    }

    fun green(): Int {
        return colorChannel(android.graphics.Color::green)
    }

    fun blue(): Int {
        return colorChannel(android.graphics.Color::blue)
    }

    private fun colorChannel(channelExtractor: (Int) -> Int): Int {
        return channelExtractor(androidColorCode() ?: Color.WHITE)
    }
}