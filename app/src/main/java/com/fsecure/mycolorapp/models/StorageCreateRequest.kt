package com.fsecure.mycolorapp.models

data class StorageCreateRequest(val data: List<Color>)