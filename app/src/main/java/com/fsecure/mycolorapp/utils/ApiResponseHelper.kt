/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.fsecure.mycolorapp.models.ResponseObject
import com.fsecure.mycolorapp.utils.Constant.RETRY_STATUS_CODES

class ApiResponseHelper {
    companion object {
        fun <T> handleApiResponse(
            context: Context,
            responseObject: ResponseObject<T>,
            listener: (T) -> Unit
        ) {
            if (responseObject.data != null) {
                listener(responseObject.data)
            } else if (responseObject.statusCode == 401) {
                showDialog(context, responseObject.apiException!!, "Authentication failed")
            } else if (responseObject.statusCode == 403) {
                showDialog(context, responseObject.apiException!!, "Unauthorized")
            } else if (RETRY_STATUS_CODES.contains(responseObject.statusCode)) {
                showDialog(
                    context,
                    "Server is temporary unavailable, please try again after few minutes",
                    "Server is unavailable"
                )
            } else {
                showDialog(context, "Server error")
            }
        }

        fun showDialog(context: Context, message: String, title: String? = null) {
            AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(android.R.string.ok, { dialog, which ->
                    dialog.dismiss()
                })
                .show()
        }
    }
}