/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey

class AppPreference(val context: Context) {
    companion object {
        private var appPreference: AppPreference? = null
        fun getInstance(context: Context): AppPreference {
            if (appPreference == null) {
                appPreference = AppPreference(context.applicationContext)
            }
            return appPreference!!
        }
    }

    private val APP_SHARED_PREFS = "com.fsecure.mycolorapp"
    private var appSharedPrefs: SharedPreferences
    private var prefsEditor: SharedPreferences.Editor

    enum class Keys {
        TOKEN,
        DATA_ID
    }

    init {
        val masterKey = MasterKey.Builder(context).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()
        appSharedPrefs = EncryptedSharedPreferences.create(
            context,
            APP_SHARED_PREFS,
            masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

        this.prefsEditor = appSharedPrefs.edit()
    }

    fun putString(key: String, text: String) {
        prefsEditor.putString(key, text)
        prefsEditor.commit()
    }

    fun getString(str: Keys): String {
        return appSharedPrefs.getString(str.name, "")!!
    }

    fun getLong(key: String): Long {
        return appSharedPrefs.getLong(key, 0)

    }

    fun putLong(key: String, value: Long) {
        prefsEditor.putLong(key, value)
        prefsEditor.commit()
    }


}