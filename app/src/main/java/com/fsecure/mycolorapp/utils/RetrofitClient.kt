/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.utils

import com.fsecure.mycolorapp.utils.Constant.RETRY_STATUS_CODES
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient {
    companion object {
        private var retrofit: Retrofit? = null

        //        private const val BASE_URL = "https://54t9f06ot1.execute-api.eu-central-1.amazonaws.com/api/"
        private const val BASE_URL = "https://id24jneh2i.execute-api.eu-west-1.amazonaws.com/api/"

        internal const val RETRY_COUNT = 10

        var gson = GsonBuilder()
            .setLenient()
            .create()

        val retryInterceptor = Interceptor {
            val request = it.request()
            var response = it.proceed(request)
            var count = 0
            while (RETRY_STATUS_CODES.contains(response.code()) && count < RETRY_COUNT) {
                response = it.proceed(request)
                count++
            }
            response
        }

        fun getInstance(): Retrofit {
            if (retrofit == null) {
                val okHttpClient = OkHttpClient.Builder().addInterceptor(retryInterceptor).build()

                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
            }
            return retrofit!!
        }
    }
}