package com.fsecure.mycolorapp.utils

import android.view.View
import androidx.databinding.BindingAdapter

object BindingAdapters {

    @JvmStatic
    @BindingAdapter("visibility")
    fun View.setVisibility(visibilityBool: Boolean) {
        visibility = if (visibilityBool) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

}