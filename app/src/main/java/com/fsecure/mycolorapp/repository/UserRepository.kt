/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.repository

import androidx.lifecycle.MutableLiveData
import com.fsecure.mycolorapp.`interface`.UserService
import com.fsecure.mycolorapp.models.ResponseObject
import com.fsecure.mycolorapp.utils.RetrofitClient
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepository {
    private var userService: UserService = RetrofitClient.getInstance().create(UserService::class.java)

    companion object {
        const val USERNAME_KEY = "username"
        const val PASSWORD_KEY = "password"
        const val TOKEN_KEY = "token"
        private var userRepository: UserRepository? = null
        fun getInstance(): UserRepository {
            if (userRepository == null) {
                userRepository = UserRepository()
            }
            return userRepository!!
        }
    }

    fun login(username: String, password: String): MutableLiveData<ResponseObject<String>> {
        var responseData: MutableLiveData<ResponseObject<String>> = MutableLiveData()
        var userCredentials = hashMapOf(USERNAME_KEY to username, PASSWORD_KEY to password)
        userService.login(userCredentials).enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                responseData.postValue(ResponseObject(null, t.message))
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful) {
                    responseData.postValue(
                        ResponseObject(
                            response.body()?.get(TOKEN_KEY)?.asString,
                            statusCode = response.code()
                        )
                    )
                } else {
                    responseData.postValue(
                        ResponseObject(
                            null,
                            response.message(),
                            response.code()
                        )
                    )
                }
            }
        })
        return responseData
    }

}