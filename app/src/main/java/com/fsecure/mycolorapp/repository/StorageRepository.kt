/*
 * Copyright (c) 2021 F-secure Corporation.
 */

package com.fsecure.mycolorapp.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.fsecure.mycolorapp.R
import com.fsecure.mycolorapp.`interface`.StorageService
import com.fsecure.mycolorapp.models.*
import com.fsecure.mycolorapp.utils.AppPreference
import com.fsecure.mycolorapp.utils.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StorageRepository(val context: Context) {
    private var storageService: StorageService
    private var appPreference: AppPreference

    companion object {
        private var storageRepository: StorageRepository? = null
        const val DATA_KEY = "data"
        fun getInstance(context: Context): StorageRepository {
            if (storageRepository == null) {
                storageRepository = StorageRepository(context.applicationContext)
            }
            return storageRepository!!
        }
    }

    init {
        storageService = RetrofitClient.getInstance().create(StorageService::class.java)
        appPreference = AppPreference.getInstance(context)
    }

    fun getStorage(id: Long): MutableLiveData<ResponseObject<Storage>> {
        val responseData: MutableLiveData<ResponseObject<Storage>> = MutableLiveData()
        val token = appPreference.getString(AppPreference.Keys.TOKEN)
        storageService.getStorage(context.resources.getString(R.string.content_type), token, id)
            .enqueue(object : Callback<Storage> {
                override fun onFailure(call: Call<Storage>, t: Throwable) {
                    responseData.postValue(ResponseObject(null, t.message))
                }

                override fun onResponse(call: Call<Storage>, response: Response<Storage>) {
                    if (response.isSuccessful) {
                        responseData.postValue(
                            ResponseObject(
                                response.body(),
                                statusCode = response.code()
                            )
                        )
                    } else {
                        responseData.postValue(
                            ResponseObject(
                                null,
                                response.message(),
                                response.code()
                            )
                        )
                    }
                }
            });
        return responseData;
    }

    fun createStorage(colors: List<Color>): MutableLiveData<ResponseObject<Storage>> {
        val responseData: MutableLiveData<ResponseObject<Storage>> = MutableLiveData()
        val token = appPreference.getString(AppPreference.Keys.TOKEN)
        val data = StorageCreateRequest(colors)
        storageService.createStorage(
            context.resources.getString(R.string.content_type),
            token,
            data
        ).enqueue(object : Callback<Storage> {
            override fun onFailure(call: Call<Storage>, t: Throwable) {
                responseData.postValue(ResponseObject(null, t.message))
            }

            override fun onResponse(call: Call<Storage>, response: Response<Storage>) {
                if (response.isSuccessful) {
                    responseData.postValue(
                        ResponseObject(
                            response.body(),
                            statusCode = response.code()
                        )
                    )
                } else {
                    responseData.postValue(
                        ResponseObject(
                            null,
                            response.message(),
                            response.code()
                        )
                    )
                }
            }

        })

        return responseData
    }

    fun updateStorage(id: Long, colors: List<Color>): MutableLiveData<ResponseObject<Storage>> {
        val responseData: MutableLiveData<ResponseObject<Storage>> = MutableLiveData()
        val token = appPreference.getString(AppPreference.Keys.TOKEN)
        val data = StorageUpdateRequest(colors)
        storageService.updateStorage(
            context.resources.getString(R.string.content_type),
            token,
            data,
            id
        ).enqueue(object : Callback<Storage> {
            override fun onResponse(call: Call<Storage>, response: Response<Storage>) {
                if (response.isSuccessful) {
                    responseData.postValue(
                        ResponseObject(
                            response.body(),
                            statusCode = response.code()
                        )
                    )
                } else {
                    responseData.postValue(
                        ResponseObject(
                            null,
                            response.message(),
                            response.code()
                        )
                    )
                }
            }

            override fun onFailure(call: Call<Storage>, t: Throwable) {
                responseData.postValue(ResponseObject(null, t.message))
            }

        })
        return responseData;
    }

}